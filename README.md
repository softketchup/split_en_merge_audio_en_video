[![Watch the video](https://codeberg.org/softketchup/split_en_merge_audio_en_video/raw/branch/main/1.png)](https://codeberg.org/softketchup/split_en_merge_audio_en_video/raw/branch/main/Dance_with_Guano.mp4)

## Sources

* [Extract Audio from Video using Python](https://copyassignment.com/extract-audio-from-video-using-python/)
* [Merge Audio and Video Files using Python](https://mlhive.com/2023/04/merge-audio-and-video-files-using-python)
* [How to embed a Video into GitHub README.md (Markdown)](https://bobbyhadz.com/blog/embed-video-into-github-readme-markdown)